const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth.js");

// Adding courses
	router.post("/", auth.verify, courseControllers.addCourse);

// Get all active courses
	router.get("/allActiveCourses", courseControllers.getAllActive);

// Get all course -- ask kung bakit ganto ito or bakit dapat andito ito
	router.get("/allCourses", auth.verify, courseControllers.getAllCourses);

// Get specific course
	router.get("/:courseId", courseControllers.getCourse);

// Update specific course
	router.put("/update/:courseId", auth.verify, courseControllers.updateCourse);

// Update specific field course
	router.patch("/archive/:courseId", auth.verify, courseControllers.updateSpecificFieldCourse);



module.exports = router; 