const express = require("express");
const router = express.Router();
const auth = require("../auth")

const userController = require("../controllers/userControllers")
	// Route for checking Email
		router.post("/checkEmail", userController.checkEmailExists);

	// Route for registration
		router.post("/register", userController.checkEmailExists, userController.registerUser);

	// Route for log-in
		router.post("/login", userController.loginUser);

	// Route for details
		router.post("/details", auth.verify, userController.getProfile);

	// Route for logged-in user
		router.get("/profile", userController.profileDetails);

	// Update user role
		router.patch("/updateRole/:userId", auth.verify, userController.updateRole);

	// Enroll user
		router.post("/enroll/:courseId", auth.verify, userController.enroll);


module.exports = router;