const User = require("../models/User");
const Course = require("../models/Course")
const bcrypt = require("bcrypt");
const auth = require("../auth");

// let us check if the email already exists
/*
	Steps:
		1. Use mongoose "find" method to find duplicate emails.
		2. Use the "then" method to send a response to the front end application bases on the result of the find method.
*/

// this part checks if the request email is existing already. 
module.exports.checkEmailExists = (request, response, next) => {
	// the result is sent back to the frontEnd via the .then method.
		// finde method returns an array of record of matching documents
	return User.find({email: request.body.email}).then(result => {
		let message = `${request.email}`;
		if(result.length > 0){
			message = `The ${request.body.email} is already taken. Please use other email.`
			return response.send(message);

		}else{
			next();
		}
	})
}


module.exports.registerUser = (request, response) =>{

	// creates variable "newUser" and instantiates a new 'User' object using mongoose model
	// Uses the confirmation mongoose model
	let newUser = new User({
		firstName: 	request.body.firstName,
		lastName: 	request.body.lastName,
		email: 		request.body.email,

		password: 	bcrypt.hashSync(request.body.password, 10),
		mobileNo: 	request.body.mobileNo,

	})

	// destructuring this above code 
		//const { firstName, lastName, email, password, mobileNo } = req.body;


	// saves the created object to our database
	return newUser.save().then(user => {
		console.log(user);
		response.send(`Congratulations ${newUser.firstName}, You are now successfully Registered!`)
	}).catch(error => {
		console.log(`Sorry ${newUser.firstName}, there was an error during the registration. Please try again.`)
	})
}

// User Aunthentication
/*
	Steps:
	1. Check database if the user email exists.
	2. compare the password provided in the login form with the password stored in the database.
*/

module.exports.loginUser = (request, response) => {
	// the findOne() method returns the first record in the collection that matches the search criteria.

	return User.findOne({email : request.body.email})
	.then(result => {
		if(result == null){
			response.send(`Your email: ${request.body.email} doesn't exists. Register first!`)
		}else{
			// creates the varialbe "isPasswordCorrect" to return the result of the comaparing the login form password and the database password.
			// the compareSync method is used to compare a non encrypted password from the login from to the encrypted password retrieve. It will return true or false value depending on the result.

			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			let token = auth.createAccessToken(result);
			console.log(token);

			if(isPasswordCorrect){

				return response.send({accessToken: token});
			}else{
				return response.send(`Incorrect password. Please try again.`)
			}
		}

	})

}


module.exports.getProfile = (request, response) => {
	return User.findOne({id: request.body.id})
	.then(result => {

		if(result.id !== request.body.id){
			response.send(`Id doesn't exists. Please try again!`);

		}else{
			result.password = "";
			response.send(result);

		}

	})
}

/* more simpler way of coding the code above

	module.exports.getProfile = (request, response) =>{

		return User.findById(request.body.id).then(result => {
			result.password = "******";
			console.log(result);
			return response.send(result);

		}).catch(error => {
			console.log(error);
			return response.send(error);

		})
}

*/

module.exports.profileDetails = (request, response) => {
	// user will be object that containe the id and email of the user that is currently logged in.
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result => {
		result.password = "Confidential";
		return response.send(result);
	}).catch(err => {
		return response.send(err);
	})
}


// Update role (is admin or not admin)

module.exports.updateRole = (request, response) => {
	let token = request.headers.authorization;
	let userData = auth.decode(token);

	// idToBeUpdated is the users id whose role we are about to change
	let idToBeUpdated = request.params.userId;

	if(userData.isAdmin){
		// return User.findByIdAndUpdate(idToBeUpdated, isAdmin:!isAdmin).then()

		return User.findById(idToBeUpdated).then(result => {

			let update = {
				isAdmin : !result.isAdmin
			}

			return User.findByIdAndUpdate(idToBeUpdated, update, {new: true}).then(document => {
					result.password = "Confidential."
					response.send(document)

				}).catch(err => response.send(err))
		
		}).catch(err => response.send(err));

	}else{
		return response.send("You don't have access to this page!")
	}
}


// Enrollment

module.exports.enroll = async (request, response) => {
	let token = request.headers.authorization;
	let userData = auth.decode(token);
	let courseId = request.params.courseId;

	if(!userData.isAdmin){
		let data = {
			courseId : courseId, // from models/Course.js
			userId : userData.id // from models/Users/js
		}

	// Enrolled user on the Course (minus one every user enrolled)
		let isCourseUpdated = await Course.findById(courseId).then(result => {
			result.enrollees.push({
				userId : data.userId
			})
			result.slots -= 1;

			return result.save().then(success => {
				return true;
			}).catch(error => false)
				
		}).catch(error => response.send(false))

	// Inserting courses enrolled to the user
		let isUserUpdated = await User.findById(data.userId).then(result =>{
			result.enrollments.push({
				courseId : data.courseId
			})

			return result.save().then(success => {
				return true;
			}).catch(error =>  false)

		}).catch(error => response.send(false))


	// Check if all changes are made or successful
		return (isUserUpdated && isCourseUpdated) ? response.send("You are now Enrolled!") : response.send("We encountered a problem in your enrollment. Please try again!")

	}else{
		return response.send("You're an Admin, you cannot enroll to any course.")
	}
}