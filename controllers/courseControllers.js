const Course = require("../models/Course");
const auth = require("../auth");


/*
	steps:
	1. Create a new Course object using mongoose model and information from the request of the user.
	2. Save a new course to the new database.
*/

// const userData = auth.decode(request.headers.authorization);
// 	console.log(userData);

// 	return User.findById(userData.id).then(result => {
// 		result.password = "Confidential";
// 		return response.send(result);
// 	}).catch(err => {
// 		return response.send(err);
// 	})


module.exports.addCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	let newCourse = new Course({
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		slots: request.body.slots,
	})

	if(userData.isAdmin){
		newCourse.save().then(result => {
			console.log(result);
			response.send(`${true}. Course successfully added!`);
		}).catch(error => {
			console.log(error);
			response.send(`${false}. Error found!`);
		})

	}else{
		response.send(`${false}. You are not an Admin!`);
	}

	
}


// Retrieve all active courses

module.exports.getAllActive = (request, response) => {
	return Course.find({isActive: true})
	.then(result => {
		response.send(result);
	}).catch(err => {
		response.send(err);
	})
}


// Retrieving specific course

module.exports.getCourse = (request, response) => {
	const courseId = request.params.courseId;

	return Course.findById(courseId).then(result => {
		response.send(result);
	}).catch(error => {
		response.send(error)
	})
}

// Updating a course

module.exports.updateCourse = (request, response) => {
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	console.log(userData);

	let updatedCourse = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		slots: request.body.slots,
	}

	const courseId = request.params.courseId;

	if(userData.isAdmin){
		return Course.findByIdAndUpdate(courseId, updatedCourse, {new: true}).then(result => {
			response.send(result);

		}).catch(error => {
			response.send(error);
		})

	}else{
		return response.send("You don't have an access to this page!");
	}
}

// Updating an specific field course

module.exports.updateSpecificFieldCourse = (request, response) => {
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	let updatedCourse = {
		isActive: request.body.isActive,
	}

	const courseId = request.params.courseId;

	if(userData.isAdmin){
		return Course.findByIdAndUpdate(courseId, updatedCourse, {new: true}).then(result => {
			response.send(`The course is Archived successfully: ${true}`);

		}).catch(error => {
			response.send(error);
		})

	}else{
		return response.send("You don't have an access to this page!");
	}
}


// Retrieving all course including those inactive course

/*
	Steps:
		1. Retrieve all the courses (active/ inactive) from the database.
		2. Verify the role of the current user(admin to continue).
*/

module.exports.getAllCourses = (request, response) => {
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	// other way of using if truethy/falsy value by negating it.
	if(!userData.isAdmin){
		return response.send("Sorry, you don't have acces to this page.");
	}else{
		return Course.find({}).then(result => response.send(result))
		.catch(error => {
			console.log(error);
			response.send(error);
		})
	}
}
