// Set-up Dependencies

const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors');

// allows access to routes defined within
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes")
const app = express();

// MIDDLEWARES

  // allowing all resources to access our backend application
  app.use(cors());
  app.use(express());

  // express.json() - built-in middleware function
  app.use(express.json());
  app.use(express.urlencoded({extended:true}));
  
  // defines the "/users" to be included for all user routes defined in the userRoutes route file.
  app.use("/users", userRoutes);
  app.use("/courses", courseRoutes);


// Database connection
	mongoose.connect("mongodb+srv://admin:admin@zuittbatch243talinio.zbprtxo.mongodb.net/s38-d-bookingAPI?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

	mongoose.connection.on("error", console.error.bind(console, "connection error"));
	mongoose.connection.once('open',() => console.log("Now connected to MongoDB Atlas"));

// this syntax will allow flexibility when using the application both locally or as a hosted app
// process.env.PORT is that can be assigned by your hosting service
	app.listen(process.env.PORT || 4000, () =>{
		console.log(`API is now Online on port ${process.env.PORT || 4000}`);
	})


