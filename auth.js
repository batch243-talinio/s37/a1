// We require the jsonwebtoken module and then contain it in jwt
const jwt = require("jsonwebtoken");

// used in algorithm for encrypting our data which makes it difficult to decode the information without defined secret key.
const secret = "CourseBookingAPI";

// [Section] JSON Web token
/*
	Analogy:
		Pack the gift provided with a lock, which can only be opened using the secret code as the key
*/

module.exports.createAccessToken = (result) => {
	// payload of the JWT
	const data = {
		id: result._id,
		email: result.email,
		isAdmin: result.isAdmin
	}

	// generate a JSON web token using the jwt's sign method.
	/*
		Syntax:
			jwt.sign(payload, secretOrPrivateKey,[callbackfunction])
	*/

	return jwt.sign(data, secret, {});
}

// Token Verification

/*
	Analogy:
		- it's like you recieved a gift and opened the lock to verify if the sender is legitimate and the gift was not tampered.
*/

module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization;
	console.log(token);

	if(token !== undefined){
		// Validitate the "token" using verify method, to decrypt the token using the secret code.
		/*
			Syntax:
				jwt.verify(token, seccret, [callback function])
		*/
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error) => {
			if(error){
				return response.send(`Invalid Token!`)
			}else{
				next();
			}
		})

	}else{
		return response.send(`Authentication failed! No Token provided.`)
	}


}


// Token Decryption
/*
	Analogy:
		- it's like unwrapping presents/gift
*/

module.exports.decode = (token) => {
	if(token === undefined){
		return null;

	}else{
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return null;

			}else{
				// decode method is used to obtaine the information from the JWT.
				// syntax: jwt.decode(token, {options})
				// it will return an object with the access to the payload property,
				// an auth data is contained in 3 parts which as a whole is an object
					//1. header, 2.payload, 3.(i forgot)
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
}



